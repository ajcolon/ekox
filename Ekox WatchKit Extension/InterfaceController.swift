//
//  InterfaceController.swift
//  Ekox WatchKit Extension
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    @IBOutlet weak var btn1: WKInterfaceButton!
    @IBOutlet weak var btn2: WKInterfaceButton!
    @IBOutlet weak var btn3: WKInterfaceButton!
    @IBOutlet weak var btn4: WKInterfaceButton!
    @IBOutlet weak var btn5: WKInterfaceButton!
    @IBOutlet weak var btn6: WKInterfaceButton!
    @IBOutlet weak var btn7: WKInterfaceButton!
    @IBOutlet weak var btn8: WKInterfaceButton!
    @IBOutlet weak var btn9: WKInterfaceButton!
    
    //Game Objects
    var memory:[Int] = []
    var tmpmemory:[Int] = []
    var buttonsArray:[WKInterfaceButton] = []
    var currState:States = States.WAIT
    var playerPoints:Int = 0;
    @IBOutlet weak var lblInstructions: WKInterfaceLabel!
    
    enum States {
        case DISPLAY, READ, WAIT, FAIL, CORRECT
    }
    
    enum PLANETINDEX: Int {
        case MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE, PLUTO
        
        static let planetNames = [
            MERCURY : "mercury", VENUS : "venus", EARTH : "earth",
            MARS : "mars", JUPITER : "jupiter", SATURN : "saturn",
            URANUS : "uranus", NEPTUNE : "neptune", PLUTO : "pluto"]
        
        func planetName() -> String {
            if let planetName = PLANETINDEX.planetNames[self] {
                return planetName
            } else {
                return "UnknownPlanet"
            }
        }
        
        func planetImage(isHighlighted:Bool) -> String {
            var path:String = ""
            if(isHighlighted == false){
                path = "\(planetName())"
            }else{
                path = "\(planetName())_selected"
            }
            
            return path
        }
    }
    
    @IBAction func startGame() {
        println("Starting Game")
        
        //append new random number
        self.appendNewNum()
        
        //display memory
        self.displayMemory()
    }
    
    @IBAction func cancelGame() {
        println("Cancel Game")
        self.loadFailScreen()
    }
    
    func setPlanetsInOrbit(){
        for (var i = 0 ; i < 9;i++){
            let planetIndex = PLANETINDEX(rawValue: i)
            buttonsArray[i].setBackgroundImage(UIImage(named: planetIndex!.planetImage(false)))
        }
    }
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        
        self.setTitle("Ekox");
        self.lblInstructions.setText("Force Touch to Start");
        super.willActivate()
        loadButtons()
        setPlanetsInOrbit()
    }
    
    func appendNewNum(){
        let randomNum = Int(arc4random_uniform(9) + 1)
        memory.append(randomNum)
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func displayMemory(){
        //self.setTitle("Follow me");
        self.lblInstructions.setText("Follow me");
        
        self.currState = States.DISPLAY
        
        var delayTime:Double = 1.0
        
        for(var i = 0; i < memory.count; i++ ){
            
            var tmpButton = self.buttonsArray[self.memory[i]-1]
            
            
            let delay = 3.0 * Double(NSEC_PER_SEC)
            let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
            let planetIndex = PLANETINDEX(rawValue: self.memory[i]-1)
            self.delay(delayTime++){
                tmpButton.setBackgroundImage(UIImage(named: planetIndex!.planetImage(true)))
                
            }
            self.delay(delayTime++){
                tmpButton.setBackgroundImage(UIImage(named: planetIndex!.planetImage(false)))
            }
            
        }
        self.delay(delayTime){
            self.getUserInput()
        }
        
        
    }
    
    @IBAction func btn1click() {
        if(currState == States.READ){
            checkMemory(1)
        }
    }
    @IBAction func btn2click() {
        if(currState == States.READ){
            checkMemory(2)
        }
    }
    @IBAction func btn3click() {
        if(currState == States.READ){
            checkMemory(3)
        }
    }
    @IBAction func btn4click() {
        if(currState == States.READ){
            checkMemory(4)
        }
    }
    @IBAction func btn5click() {
        if(currState == States.READ){
            checkMemory(5)
        }
    }
    @IBAction func btn6click() {
        if(currState == States.READ){
            checkMemory(6)
        }
    }
    @IBAction func btn7click() {
        if(currState == States.READ){
            checkMemory(7)
        }
    }
    @IBAction func btn8click() {
        if(currState == States.READ){
            checkMemory(8)
        }
    }
    @IBAction func btn9click() {
        if(currState == States.READ){
            checkMemory(9)
        }
    }
    
    
    func checkMemory(pattern: Int){
        if(pattern == self.tmpmemory[0]){
            self.tmpmemory.removeAtIndex(0)
        }else{
            self.lblInstructions.setText("You Failed");
            loadFailScreen()
        }
        
        //check temp memory count
        if(tmpmemory.count == 0){
            self.goToNextTurn()
        }
    }
    
    func loadFailScreen(){
        presentControllerWithName("MenuScreen", context: self.playerPoints)
    }
    
    func goToNextTurn(){
        self.currState = States.CORRECT
        self.appendNewNum()
        self.displayMemory()
        self.playerPoints++;
        self.sendPointsToGC()
    }
    
    func sendPointsToGC(){
        let data = ["score":self.playerPoints];
        WKInterfaceController.openParentApplication(data,
            reply: {(reply, error) -> Void in
                println("openParentApplication called in button function")
        })
    }
    
    func getUserInput(){
        self.tmpmemory = self.memory
        self.lblInstructions.setText("Your Turn");
        currState = States.READ
    }
    
    func loadButtons(){
        buttonsArray.append(btn1)
        buttonsArray.append(btn2)
        buttonsArray.append(btn3)
        buttonsArray.append(btn4)
        buttonsArray.append(btn5)
        buttonsArray.append(btn6)
        buttonsArray.append(btn7)
        buttonsArray.append(btn8)
        buttonsArray.append(btn9)
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
}