//
//  MenuScene.swift
//  Ekox
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//
import WatchKit
import Foundation

class MenuScreen: WKInterfaceController {
    
    @IBOutlet weak var lblScore: WKInterfaceLabel!
    @IBOutlet weak var lblRecord: WKInterfaceLabel!
    override func willActivate() {
        println("willActivate")
        super.willActivate()
    }
    
    //    override func init(context: AnyObject?) {
    //        super.init(context: context)
    //    }
    
    init(context:AnyObject!) {
        
        super.init()
        var points = context as! Int!
        
        let defaults = NSUserDefaults.standardUserDefaults()
        var topPoints: Int? = defaults.objectForKey("TopPoints") as? Int
        if(topPoints == nil){
            topPoints = 0
        }
        
        self.lblRecord.setText("Record Score: \(topPoints!)")
        self.lblScore.setText("Score: \(points)")
        
        if(points > topPoints){
            defaults.setObject(points, forKey: "TopPoints")
            defaults.synchronize()
            self.lblRecord.setText("Record: \(points)")
        }
        
    }
    
    override func didDeactivate() {
        super.didDeactivate()
    }
    
    @IBAction func playGame() {
        dismissController()
    }
}