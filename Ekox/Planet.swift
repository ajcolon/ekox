//
//  Planet.swift
//  Ekox
//
//  Created by Albith Joel Colon on 4/26/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import Foundation
import SpriteKit;

class Planet:SKSpriteNode{
    
//    let bgIdleBox = "Button";
//    let bgActiveBox = "ButtonHighlited";
    var myIndex:PLANETINDEX!
    
    enum PLANETINDEX: Int {
        case MERCURY, VENUS, EARTH, MARS, JUPITER, SATURN, URANUS, NEPTUNE, PLUTO
        
        static let planetNames = [
            MERCURY : "Mercury", VENUS : "Venus", EARTH : "Earth",
            MARS : "Mars", JUPITER : "Jupiter", SATURN : "Saturn",
            URANUS : "Uranus", NEPTUNE : "Neptune", PLUTO : "Pluto"]
        
        func planetName() -> String {
            if let planetName = PLANETINDEX.planetNames[self] {
                return planetName
            } else {
                return "UnknownPlanet"
            }
        }
        
        func planetImage(isHighlighted:Bool) -> String {
            var path:String = ""
            if(isHighlighted == false){
                path = "\(planetName())"
            }else{
                path = "\(planetName())_selected"
            }
            
            return path
        }
    }
    
    
    init(id:Int){
        let planetIndex = PLANETINDEX(rawValue: id)
        self.myIndex = planetIndex
        let imageTexture = SKTexture(imageNamed: planetIndex!.planetImage(false));
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
        self.xScale = 0.1;
        self.yScale = 0.1;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggleTexture(isDisplay:Bool){
        //let changeTexture =
        
        self.runAction(SKAction.playSoundFileNamed("blurp_x.wav", waitForCompletion: false))
        
        
        let changeTexture = SKAction.setTexture(SKTexture(imageNamed: self.myIndex.planetImage(true)))
        changeTexture.timingMode = SKActionTimingMode.EaseInEaseOut;
        
        let scaleDown = SKAction.scaleBy(0.2, duration: 0.1)
        scaleDown.timingMode = SKActionTimingMode.EaseInEaseOut;
        let scaleBack = scaleDown.reversedAction()
        scaleBack.timingMode = SKActionTimingMode.EaseInEaseOut;
        
        
        
        let waitDuration = SKAction.waitForDuration(0.5)
        
        let changeTextureIdle = SKAction.setTexture(SKTexture(imageNamed: self.myIndex.planetImage(false)))
        changeTextureIdle.timingMode = SKActionTimingMode.EaseIn;
        
        var textureSequence:SKAction;
        
        if(isDisplay){
            textureSequence = SKAction.sequence([waitDuration,changeTexture,waitDuration,changeTextureIdle]);
        }else{
            textureSequence = SKAction.sequence([scaleDown,scaleBack,changeTexture,waitDuration,changeTextureIdle]);
        }
        
        self.runAction(textureSequence)
    }
    
}