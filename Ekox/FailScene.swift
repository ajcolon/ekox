//
//  FailScene.swift
//  Ekox
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import SpriteKit

class FailScene: SKScene {
    var replayButton: SKSpriteNode!;
    var leadButton: SKSpriteNode!;
    var points:Int = 0
    var lblPoints:SKLabelNode = SKLabelNode(fontNamed: "AvenirNext-Medium")
    var lblTopPoints:SKLabelNode = SKLabelNode(fontNamed: "AvenirNext-Medium ")
    
    override func didMoveToView(view: SKView) {
        
        println("Loading Fail Scene");
        self.loadFailInterface()
        
    }
    
    func loadFailInterface(){
        
         //Add Background Image
        var backgroundImage = SKSpriteNode(imageNamed: "PatternsBG")
        backgroundImage.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        backgroundImage.zPosition = -1
        backgroundImage.xScale = 0.5
        backgroundImage.yScale = 0.5
        self.addChild(backgroundImage)
        
        //ekoxPlanet
        let ekoxPlanet = SKSpriteNode(imageNamed: "ekoxPlanetCry")
        ekoxPlanet.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMinY(self.frame)+20)
        ekoxPlanet.size.width = self.size.width - 200
        ekoxPlanet.size.height = self.view!.bounds.height/2
        ekoxPlanet.xScale = 0.6
        ekoxPlanet.yScale = 0.6
        self.addChild(ekoxPlanet)
        
        
        //Add Play Button
        self.replayButton = SKSpriteNode(imageNamed: "ButtonReplay")
        self.replayButton.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+200)
        //self.playButton.size.width = self.size.width - 100
        self.replayButton.xScale = 0.30
        self.replayButton.yScale = 0.30
        
        self.addChild(self.replayButton)
        
        //Add Leaderboard Button
        self.leadButton = SKSpriteNode(imageNamed: "ButtonLeaderboards")
        self.leadButton.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+100)
        //self.playButton.size.width = self.size.width - 100
        self.leadButton.xScale = 0.30
        self.leadButton.yScale = 0.30
        
        self.addChild(self.leadButton)
        
        
        //Add Current Points Label
        self.lblPoints.text = "Current Points: \(points)"
        self.lblPoints.fontSize = 20
        self.lblPoints.fontColor = UIColor(netHex:0x26AEFC) //#26AEFC
        self.lblPoints.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame) - 100)
        self.addChild(self.lblPoints)
        
        //Add Top Points Label
        let defaults = NSUserDefaults.standardUserDefaults()
        var topPoints: Int? = defaults.objectForKey("TopPoints") as? Int
        if(topPoints == nil){
            topPoints = 0
        }
        self.lblTopPoints.text = "Max Points: \(topPoints!)"
        self.lblTopPoints.fontSize = 20
        self.lblTopPoints.fontColor = UIColor(netHex:0x26AEFC) //#26AEFC
        self.lblTopPoints.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMaxY(self.frame) - 120)
        self.addChild(self.lblTopPoints)
        
        //Show / Set user preferecnes
        if(self.points > topPoints){
            defaults.setObject(points, forKey: "TopPoints")
            defaults.synchronize()
            self.lblTopPoints.text = "Max Points: \(points)"
        }
    }
    
    func showLeaderboard(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("EKOX_ShowLeaderboard", object: nil, userInfo: nil);
        })
    }
    
    func loadGameScene(){
        println("Lets Play");
        
        let transition = SKTransition.crossFadeWithDuration(1.0)
        
        let scene = GameScene(size: self.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill
        self.view?.presentScene(scene, transition: transition)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self);
            if(self.nodeAtPoint(location) == self.replayButton){
                self.loadGameScene()
                
            }else if(self.nodeAtPoint(location) == self.leadButton){
                self.showLeaderboard()
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
