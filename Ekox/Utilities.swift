//
//  Utilities.swift
//  Ekox
//
//  Created by Albith Joel Colon on 5/5/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import Foundation
import SpriteKit

class Utilities {
    func random()->UInt32{
        var range = UInt32(50)...UInt32(200)
        return range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1)
    }
    func random(#initRange:UInt32, finalRange:UInt32)->UInt32{
        var range = UInt32(initRange)...UInt32(finalRange)
        return range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1)
    }
    
    func random(#initRange:UInt32, finalRange:UInt32)->CGFloat{
        var range = UInt32(initRange)...UInt32(finalRange)
        return CGFloat(range.startIndex + arc4random_uniform(range.endIndex - range.startIndex + 1));
    }
    func randomPointOnCircle(radius:Float, center:CGPoint) -> CGPoint {
        // Random angle in [0, 2*pi]
        let theta = Float(arc4random_uniform(UInt32.max))/Float(UInt32.max) * Float(M_PI) * 2.0
        // Convert polar to cartesian
        let x = radius * cosf(theta)
        let y = radius * sinf(theta)
        return CGPointMake(CGFloat(x)+center.x,CGFloat(y)+center.y)
    }
}