//
//  PatternBox.swift
//  Patterns
//
//  Created by Albith Joel Colon on 4/6/15.
//  Copyright (c) 2015 Albith Joel Colon. All rights reserved.
//

import Foundation
import SpriteKit;

class PatternBox:SKSpriteNode{
    
    let bgIdleBox = "Button";
    let bgActiveBox = "ButtonHighlited";
    
    
    init(){
        let imageTexture = SKTexture(imageNamed: self.bgIdleBox);
        super.init(texture:imageTexture, color:nil, size: imageTexture.size());
        self.xScale = 0.2;
        self.yScale = 0.2;
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func toggleTexture(isDisplay:Bool){
        //let changeTexture =
        
        self.runAction(SKAction.playSoundFileNamed("blurp_x.wav", waitForCompletion: false))
        
        
        let changeTexture = SKAction.setTexture(SKTexture(imageNamed: bgActiveBox))
        changeTexture.timingMode = SKActionTimingMode.EaseInEaseOut;
        
        let scaleDown = SKAction.scaleBy(0.2, duration: 0.1)
        scaleDown.timingMode = SKActionTimingMode.EaseInEaseOut;
        let scaleBack = scaleDown.reversedAction()
        scaleBack.timingMode = SKActionTimingMode.EaseInEaseOut;
        
        
        
        let waitDuration = SKAction.waitForDuration(0.5)
        
        let changeTextureIdle = SKAction.setTexture(SKTexture(imageNamed: bgIdleBox))
        changeTextureIdle.timingMode = SKActionTimingMode.EaseIn; 
        
        var textureSequence:SKAction;
        
        if(isDisplay){
            textureSequence = SKAction.sequence([waitDuration,changeTexture,waitDuration,changeTextureIdle]);
        }else{
            textureSequence = SKAction.sequence([scaleDown,scaleBack,changeTexture,waitDuration,changeTextureIdle]);
        }
        
        self.runAction(textureSequence)
    }
    
}