//
//  GameViewController.swift
//  Ekox
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit
import AVFoundation

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file as String, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! HomeScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class GameViewController: UIViewController, GKGameCenterControllerDelegate , AVAudioPlayerDelegate{
    var gameCenterEnabled: Bool = false
    var leaderboardIdentifier: String = ""
    let fileURL:NSURL = NSBundle.mainBundle().URLForResource("Patterns", withExtension: "mp3")!
    var bgSound:AVAudioPlayer!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authenticateLocalPlayer()
        initNotification()
        loadBGMusic()
        
        if let scene = HomeScene.unarchiveFromFile("HomeScene") as? HomeScene {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
            
            //scene.runAction(SKAction.repeatActionForever(SKAction.playSoundFileNamed("Patterns.mp3", waitForCompletion: true)))
        }
        
    }
    
    func loadBGMusic(){
        println("Loading Music")
        let fileURL:NSURL = NSBundle.mainBundle().URLForResource("Patterns", withExtension: "mp3")!
        var error : NSError?
        self.bgSound = AVAudioPlayer(contentsOfURL: self.fileURL, error: &error)
        if bgSound == nil {
            println("sound nil")
            if let e = error {
                println(e.localizedDescription)
            }
        }
        self.bgSound.prepareToPlay()
        self.bgSound.delegate = self
        self.bgSound.volume = 1.0
        self.bgSound.numberOfLoops = -1
        self.bgSound.play()
    }
    
    func initNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reportScore:", name: "EKOX_ReportScore", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "showLeaderboardAndAchievement", name: "EKOX_ShowLeaderboard", object: nil)
    }
    
    func gameCenterViewControllerDidFinish(gcViewController: GKGameCenterViewController!)
    {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func authenticateLocalPlayer(){
    
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            if((viewController) != nil) {
                self.presentViewController(viewController, animated: true, completion: nil)
            }else if (localPlayer.authenticated){
                println("Local player already authenticated");
                self.gameCenterEnabled = true
                
                localPlayer.loadDefaultLeaderboardIdentifierWithCompletionHandler({ (leaderboardIdentifier : String!, error : NSError!) -> Void in
                    if error != nil {
                        println("Error authenticating: \(error.localizedDescription)")
                    } else {
                        self.leaderboardIdentifier = leaderboardIdentifier
                    }
                })
            } else {
                self.gameCenterEnabled = false;
                println("Local player could not be authenticated, disabling GameCenter");
                
            }
        }
    }
    
    func showLeaderboardAndAchievement() {
        // declare the Game Center viewController
        var gcViewController: GKGameCenterViewController = GKGameCenterViewController()
        gcViewController.gameCenterDelegate = self
        
        gcViewController.viewState = GKGameCenterViewControllerState.Leaderboards
        
        // Remember to replace "Best Score" with your Leaderboard ID (which you have created in iTunes Connect)
        gcViewController.leaderboardIdentifier = self.leaderboardIdentifier
        // Finally present the Game Center ViewController
        self.showViewController(gcViewController, sender: self)
        self.navigationController?.pushViewController(gcViewController, animated: true)
        // self.presentViewController(gcViewController, animated: true, completion: nil)
    }
    
    func reportScore(notification:NSNotification){
        let userInfo = notification.userInfo! as Dictionary
        var score = userInfo["score"] as! Int
        
        var gScore = GKScore(leaderboardIdentifier: self.leaderboardIdentifier)
        gScore.value = Int64(score)
        
        GKScore.reportScores([gScore], withCompletionHandler: ( { (error: NSError!) -> Void in
            if (error != nil) {
                // handle error
                println("Error: " + error.localizedDescription);
            } else {
                println("Score reported: \(gScore.value)")
            }
        }))
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        println("finished playing \(flag)")
    }
    
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer!, error: NSError!) {
        println("\(error.localizedDescription)")
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
