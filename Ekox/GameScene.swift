//
//  GameScene.swift
//  Ekox
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//
import SpriteKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
}

class GameScene: SKScene {
    
    enum States {
        case DISPLAY, READ, WAIT, FAIL, CORRECT
    }
    
    var appDelegate:AppDelegate!
    var buttonsArrays:[Planet]! = [];
    var lblTimer:SKLabelNode = SKLabelNode(fontNamed: "AvenirNext-Medium")
    var lblPoints:SKLabelNode = SKLabelNode(fontNamed: "AvenirNext-Medium")
    var lblRepeats:SKLabelNode = SKLabelNode(fontNamed: "AvenirNext-Medium")
    let buttonsAmnt = 9;
    var memory:[Int] = []
    var tmpmemory:[Int] = []
    var playerPoints:Int = 0
    var playerRepeats:Int = 0
    var ufo:SKSpriteNode!
    var modalBG:SKSpriteNode!
    var repeatBtn:SKSpriteNode!
    let util:Utilities = Utilities();
    var currState:States = States.WAIT
    var emmiter:SKEmitterNode!
    var modalIsShowing:Bool = false
    
    override func didMoveToView(view: SKView) {
        
        println("Loading Game Scene");
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.loadGame()
        let util = Utilities();
    }
    
    func loadGame(){
        println("Load Game")
        
        //load ui
        self.loadUI()
        
        //show modal to run game
        self.loadPlayModal()
        
        //loadEmitter
        self.loadParticleEffect()
        
        //Run ufo animation
        self.setUfoAnim()
        
        //Run ai loop
        self.startCometGenerator()
    }
    
    func startCometGenerator(){
        var comet:SKSpriteNode = SKSpriteNode(imageNamed: "comet")
        comet.zPosition = -20
        self.addChild(comet)
        
        var wait = SKAction.waitForDuration(25);
        let cometSequence = SKAction.sequence([
            SKAction.runBlock({
                comet.position = self.getRandomInitialPosition()
                comet.xScale = 0.02
                comet.yScale = 0.02
                
                //Set move to location
                let frameMidPoint = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame));
                let frameRadius = Float(self.frame.width);
                let location:CGPoint = self.util.randomPointOnCircle(frameRadius, center: frameMidPoint)
                let diff = CGPointMake(location.x - frameMidPoint.x, location.y - frameMidPoint.y);
                let angleRadians = atan2f(Float(diff.y), Float(diff.x));
                
                comet.runAction(SKAction.sequence([
                    SKAction.fadeInWithDuration(1),
                    SKAction.rotateByAngle(CGFloat(angleRadians), duration: 1.0),
                    SKAction.moveByX(diff.x, y: diff.y, duration: 20.0),
                    SKAction.fadeOutWithDuration(1)
                    ]));
                
                
            }),wait
        ])
        let idleForever = SKAction.repeatActionForever(cometSequence);
        self.runAction(idleForever)
    }
    
    func getRandomInitialPosition()->CGPoint{
        let randXAddition:CGFloat = self.util.random(initRange: 0, finalRange: 10);
        let randYAddition:CGFloat = self.util.random(initRange: 150, finalRange: 200);
        let randomInitPosition:UInt32 = self.util.random(initRange: 0, finalRange: 3);
        var randomPoint:CGPoint;
        switch randomInitPosition{
        case 0 :
            randomPoint = CGPointMake(CGRectGetMinX(self.frame),CGRectGetMidY(self.frame));
        case 1 :
            randomPoint = CGPointMake(CGRectGetMaxX(self.frame),CGRectGetMidY(self.frame));
        case 2 :
            randomPoint = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMaxY(self.frame)+100);
        case 3 :
            randomPoint = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMinY(self.frame)-100);
        default:
            randomPoint = CGPointMake(CGRectGetMinX(self.frame),CGRectGetMidY(self.frame));
        }
        return randomPoint
    }
    
    func startGame(){
        println("Starting Game")
        
        ///append new random number
        self.appendNewNum()
        
        //display memory
        self.displayMemory()
        
        //load player repeats
        self.loadPlayerRepeats()
    }
    
    func loadPlayerRepeats(){
        let defaults = NSUserDefaults.standardUserDefaults()
       var tmpRepeats = defaults.objectForKey("PlayerRepeats") as? Int
        if(tmpRepeats == nil){
            tmpRepeats = 3
        }
        self.playerRepeats = tmpRepeats!
        self.lblRepeats.text = "x \(self.playerRepeats)"
    }
    
    func onRepeatClick(){
        if(self.playerRepeats > 0){
            self.playerRepeats -= 1
            self.syncRepeats()
            self.displayMemory()
            
            //Animate repeat button
            let scaleDown = SKAction.scaleBy(0.2, duration: 0.1)
            scaleDown.timingMode = SKActionTimingMode.EaseInEaseOut;
            let scaleBack = scaleDown.reversedAction()
            scaleBack.timingMode = SKActionTimingMode.EaseInEaseOut;
            self.repeatBtn.runAction(SKAction.sequence([scaleDown,scaleBack]))
            
        }
    }
    
    func syncRepeats(){
        self.lblRepeats.text = "x \(self.playerRepeats)"
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.playerRepeats, forKey: "PlayerRepeats")
        defaults.synchronize()
    }
    
    func loadPlayModal(){
        let color = UIColor.blackColor()
        let alphaC = color.colorWithAlphaComponent(0.8)
        self.modalBG = SKSpriteNode(color: alphaC, size: self.size)
        self.modalBG.zPosition = 999
        self.modalBG.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        self.modalBG.size = self.frame.size
        self.addChild(self.modalBG)
        
        //RUReady
        var RUReady = SKSpriteNode(imageNamed: "RUReady")
        RUReady.position = CGPoint(x:0, y:-modalBG.frame.size.height/2 + 300)
        RUReady.xScale = 0.4
        RUReady.yScale = 0.4
        self.modalBG.addChild(RUReady)
        modalIsShowing = true
    }
    
    func setUfoAnim(){
        let currX = self.ufo.position.x
        let currY = self.ufo.position.y
        
        let idleUpAction = SKAction.moveToY(currY + 10.0, duration: 1.8);
        idleUpAction.timingMode = SKActionTimingMode.EaseOut;
        
        let idleDownAction = SKAction.moveToY(currY - 8.0, duration: 1.8);
        idleDownAction.timingMode = SKActionTimingMode.EaseIn;
        
        let idleSequence = SKAction.sequence([idleUpAction,idleDownAction]);
        let idleForever = SKAction.repeatActionForever(idleSequence);
        self.ufo.runAction(idleForever);
        
    }
    
    func removeModal(){
        
        modalIsShowing = false
        
        var fadeOut = SKAction.fadeOutWithDuration(1)
        
        var removeFromScebe = SKAction.runBlock({
            self.modalBG.removeFromParent()
        })
        
        self.modalBG.runAction(SKAction.sequence([fadeOut,removeFromScebe]))
        
    }
    
    func loadParticleEffect(){
        let sparkEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("Magic", ofType: "sks")!
        
        emmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(sparkEmmitterPath as String) as! SKEmitterNode
        
        emmiter.position = CGPointMake(self.size.width/2, self.size.height/2)
        emmiter.name = "sparkEmmitter"
        emmiter.zPosition = 20
        emmiter.targetNode = self
        emmiter.particleLifetime = 0;
        emmiter.xScale = 0.6
        emmiter.yScale = 0.6
        
        self.addChild(emmiter)
    }
    
    func appendNewNum(){
        let randomNum = Int(arc4random_uniform(9) + 1)
        memory.append(randomNum)
    }
    
    func playBackgroundMusic(){
        self.runAction(SKAction.repeatActionForever(SKAction.playSoundFileNamed("Patterns.mp3", waitForCompletion: true)))
    }
    
    func displayMemory(){
        
        self.currState = States.DISPLAY
        
        //show pattern
        var actionSeq:[SKAction]=[];
        for(var i = 0; i < self.memory.count; i++ ){
            
            var wait = SKAction.waitForDuration(1);
            var tmpButton = self.buttonsArrays[self.memory[i]-1]
            
            var display = SKAction.runBlock({
                tmpButton.toggleTexture(true)
            })
            
            actionSeq.append(wait)
            actionSeq.append(display)
        }
        
        var wait = SKAction.waitForDuration(2);
        actionSeq.append(wait)
        
        var endDisplay = SKAction.runBlock({
            self.currState = States.READ;
            self.tmpmemory = self.memory;
        })
        actionSeq.append(endDisplay)
        
        self.runAction(SKAction.sequence(actionSeq))
        
    }
    
    func loadUI(){
        
        //Add Background Image
        self.scaleMode = SKSceneScaleMode.AspectFill
        var backgroundImage = SKSpriteNode(imageNamed: "PatternsBG")
        backgroundImage.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        backgroundImage.zPosition = -999
        backgroundImage.xScale = 0.5
        backgroundImage.yScale = 0.5
        self.addChild(backgroundImage)
        
        
        //Add Buttons
        for (var i = 0; i<buttonsAmnt ; i++){
            var tmpButton = Planet(id: i)
            tmpButton.position = getButtonPosition(i)
            self.buttonsArrays.append(tmpButton)
            self.addChild(tmpButton);
        }
        
        //ufo
        self.ufo = SKSpriteNode(imageNamed: "ekoxUFO")
        self.ufo.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMinY(self.frame)+50)
        self.ufo.size = self.size
        self.ufo.xScale = 0.12
        self.ufo.yScale = 0.12
        self.addChild(self.ufo)
        
        //playerRepeatLabel
        self.lblRepeats.text = "x \(self.playerRepeats)"
        self.lblRepeats.fontSize = 25
        self.lblRepeats.fontColor = UIColor(netHex:0xFFFFFF) //#26AEFC
        self.lblRepeats.position = CGPoint(x:CGRectGetMidX(self.frame)+185, y:CGRectGetMinY(self.frame)+50)
        self.addChild(self.lblRepeats)
        
        //repeat button
        self.repeatBtn = SKSpriteNode(imageNamed: "RepeatIcon")
        self.repeatBtn.position = CGPoint(x:CGRectGetMidX(self.frame)+135, y:CGRectGetMinY(self.frame)+60)
        self.repeatBtn.size = self.size
        self.repeatBtn.xScale = 0.05
        self.repeatBtn.yScale = 0.05
        self.addChild(self.repeatBtn)
        
        //add ufo thrust
        let sparkEmmitterPath:NSString = NSBundle.mainBundle().pathForResource("shipglitter", ofType: "sks")!
        
        var shipEmmiter = NSKeyedUnarchiver.unarchiveObjectWithFile(sparkEmmitterPath as String) as! SKEmitterNode
        
        shipEmmiter.position = CGPointMake(0, -200)
        shipEmmiter.name = "shipEmmiter"
        shipEmmiter.zPosition = -20
        shipEmmiter.targetNode = self.ufo
        shipEmmiter.xScale = 0.3
        shipEmmiter.yScale = 1.0
        self.ufo.addChild(shipEmmiter)
        
        //TalkboxEmpty
        var tbx = SKSpriteNode(imageNamed: "TalkboxEmpty")
        tbx.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMinY(self.frame)+120)
        tbx.size = self.size
        tbx.xScale = 0.14
        tbx.yScale = 0.14
        self.addChild(tbx)
        
        //Add Label for Points
        self.lblPoints.text = "\(self.playerPoints)"
        self.lblPoints.fontSize = 150
        self.lblPoints.fontColor = UIColor(netHex:0x26AEFC) //#26AEFC
        self.lblPoints.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame) + 220)
        self.addChild(self.lblPoints)
        
        
        //Add Timer Label
        self.lblTimer.text = "0"
        self.lblTimer.fontSize = 22
        self.lblTimer.zPosition = 10
        self.lblTimer.fontColor = UIColor(netHex:0xE0E8EB) //#26AEFC
        self.lblTimer.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMinY(self.frame) + 120)
        self.addChild(self.lblTimer)
    }
    
    func getButtonPosition(index:Int)->CGPoint{
        var btnPoint:CGPoint
        let btnPadding:CGFloat = 130
        
        switch index {
        case 0:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)-btnPadding, y:CGRectGetMidY(self.frame)+btnPadding)
        case 1:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)+btnPadding)
        case 2:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)+btnPadding, y:CGRectGetMidY(self.frame)+btnPadding)
        case 3:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)-btnPadding, y:CGRectGetMidY(self.frame))
        case 4:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        case 5:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)+btnPadding, y:CGRectGetMidY(self.frame))
        case 6:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)-btnPadding, y:CGRectGetMidY(self.frame)-btnPadding)
        case 7:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)-btnPadding)
        case 8:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame)+btnPadding, y:CGRectGetMidY(self.frame)-btnPadding)
        default:
            btnPoint = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
        }
        return btnPoint
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        if(modalIsShowing == true){
            self.removeModal()
            var wait = SKAction.waitForDuration(4);
            var runGame = SKAction.runBlock({ self.startGame()})
            self.runAction(SKAction.sequence([wait,runGame]))
            return
        }
        
        if(currState != States.READ){return}
        
        //check for button touch
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self);
            
            if(self.nodeAtPoint(location) == self.repeatBtn){
                self.onRepeatClick()
                return
            }
            
            var index = 1
            for btn in buttonsArrays{
                if(self.nodeAtPoint(location) == btn){
                    
                    //toggle button
                    btn.toggleTexture(false);
                    
                    //check index
                    if(index == self.tmpmemory[0]){
                        self.tmpmemory.removeAtIndex(0)
                        showEmitterAtLocation(btn.position)
                        
                    }else{
                        self.runAction(SKAction.playSoundFileNamed("womp.wav", waitForCompletion: true))
                        self.loadFailScene()
                    }
                    
                    //check temp memory count
                    if(tmpmemory.count == 0){
                        self.goToNextTurn()
                    }
                }
                index++
            }
        }
    }
    
    func showEmitterAtLocation(location:CGPoint){
        self.emmiter.position = location
        let emmiterShow = SKAction.runBlock({
            self.emmiter.particleLifetime = 2.5
        });
        
        let waitDuration = SKAction.waitForDuration(0.3)
        
        let emmiterHide = SKAction.runBlock({
            self.emmiter.particleLifetime = 0
        });
        self.runAction(SKAction.sequence([emmiterShow,waitDuration,emmiterHide]))
    }
    
    func goToNextTurn(){
        self.currState = States.CORRECT
        
        var wait = SKAction.waitForDuration(2);
        
        var nextTurn = SKAction.runBlock({
            self.appendNewNum()
            self.displayMemory()
            self.playerPoints++;
            self.reportScore()
            self.checkScoreForRepeats()
            self.lblPoints.text = "\(self.playerPoints)"
        })
        
        self.runAction(SKAction.sequence([wait,nextTurn]))
    }
    
    
    //TODO: FIX THIS TO HAVE EXTENSIBLE IMPLEMENTATION
    func checkScoreForRepeats(){
        if(self.playerPoints == 25){
            self.playerRepeats += 1
        }else if(self.playerPoints == 40){
            self.playerRepeats += 3
        }
        self.syncRepeats()
    }
    
    func reportScore(){
        let scoreObject = ["score":self.playerPoints]
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName("EKOX_ReportScore", object: nil, userInfo: scoreObject);
        })
    }
    
    func loadFailScene(){
        self.currState = States.FAIL
        
        var wait = SKAction.waitForDuration(1.0);
        
        var removeEmitters = SKAction.runBlock({
            self.emmiter.removeFromParent()
            self.ufo.removeFromParent()
        })
        
        var showFailScene = SKAction.runBlock({
            let transition = SKTransition.crossFadeWithDuration(1.0)
            
            let scene = FailScene(size: self.size)
            scene.points = self.playerPoints;
            scene.scaleMode = SKSceneScaleMode.AspectFill
            self.view?.presentScene(scene, transition: transition)
        })
        
        self.runAction(SKAction.sequence([wait,showFailScene,wait,removeEmitters]))
        
    }
    
    /* Called before each frame is rendered */
    override func update(currentTime: CFTimeInterval) {
        
        switch currState{
        case States.DISPLAY:
            self.lblTimer.text = "Follow Me.."
        case States.READ:
            self.lblTimer.text = "Your Turn"
        case States.WAIT:
            self.lblTimer.text = "Wait.."
        case States.FAIL:
            self.lblTimer.text = "You Failed"
        case States.CORRECT:
            self.lblTimer.text = "Correct"
        default:
            self.lblTimer.text = ""
        }
    }
}
