//
//  HomeScene.swift
//  Ekox
//
//  Created by Albith Joel Colon Figueroa on 4/21/15.
//  Copyright (c) 2015 Apoxware. All rights reserved.
//

import SpriteKit

class HomeScene: SKScene {
    var appDelegate:AppDelegate!
    var playButton: SKSpriteNode!;
    var helloFrames:[SKTexture] = []
    var ekoxSprite:SKSpriteNode!;
    
    override func didMoveToView(view: SKView) {
        
        println("Loading Home Scene");
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        self.loadUI()
        self.loadFrames()
        
    }
    
    func loadFrames(){
        var animAtlas = SKTextureAtlas(named: "Ekox.atlas")
        var numImages = animAtlas.textureNames.count
        
        for (var i = 0; i < numImages; i++){
            var textName = "Ekox\(i)"
            var tempTexture = animAtlas.textureNamed(textName)
            self.helloFrames.append(tempTexture)
        }
        self.animateEkox()
    }
    
    func animateEkox(){
        ekoxSprite.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(self.helloFrames, timePerFrame: 0.5)))
    }
    
    func loadUI(){
        
        self.scaleMode = SKSceneScaleMode.AspectFill
        
        //Add Background Image
        var backgroundImage = SKSpriteNode(imageNamed: "PatternsBG")
        backgroundImage.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        backgroundImage.zPosition = -1
        backgroundImage.xScale = 0.5
        backgroundImage.yScale = 0.5
        self.addChild(backgroundImage)
        //backgroundImage.size = self.view?.bounds.size
        
        //ekoxPlanet
        let ekoxPlanet = SKSpriteNode(imageNamed: "ekoxPlanet")
        ekoxPlanet.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMinY(self.frame)+100)
        ekoxPlanet.xScale = 0.6
        ekoxPlanet.yScale = 0.6
        self.addChild(ekoxPlanet)
        
        //EkoxLogo
        let ekoxLogo = SKSpriteNode(imageNamed: "EkoxLogo")
        ekoxLogo.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMaxY(self.frame)-100)
        ekoxLogo.xScale = 0.30
        ekoxLogo.yScale = 0.30
        self.addChild(ekoxLogo)
        
        
        //Add Play Button
        self.playButton = SKSpriteNode(imageNamed: "ButtonPlay")
        self.playButton.position = CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+100)
        self.playButton.xScale = 0.25
        self.playButton.yScale = 0.25
        self.addChild(self.playButton)
        
        //Add Ekox
        let textureAtlas = SKTextureAtlas(named:"Ekox.atlas")
        ekoxSprite = SKSpriteNode(texture:textureAtlas.textureNamed("Ekox1"))
        ekoxSprite.xScale = 0.15
        ekoxSprite.yScale = 0.15
        ekoxSprite.position = CGPoint(x: CGRectGetMidX(self.frame)-100, y: CGRectGetMidY(self.frame)-200)
        self.addChild(ekoxSprite)
        
        //Add TalkBox
        let talkbox = SKSpriteNode(imageNamed: "Talkbox")
        talkbox.position = CGPoint(x: CGRectGetMidX(self.frame)+50, y: CGRectGetMidY(self.frame)-100)
        talkbox.xScale = 0.2
        talkbox.yScale = 0.2
        self.addChild(talkbox)
    }
    
    func loadGameScene(){
        println("Lets Play");
        
        let transition = SKTransition.crossFadeWithDuration(1.0)
        
        let scene = GameScene(size: self.size)
        scene.scaleMode = SKSceneScaleMode.AspectFill
        self.view?.presentScene(scene, transition: transition)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self);
            if(self.nodeAtPoint(location) == self.playButton){
                self.loadGameScene()
                
            }
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}